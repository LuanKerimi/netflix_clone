<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Category;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function homepage()
    {
        $categoryList = Category::all();
        $filmList = Film::all();

        return view('homepage', compact('categoryList', 'filmList'));
    }


}
