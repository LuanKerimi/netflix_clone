<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Category;
use Illuminate\Http\req;
use Symfony\Component\HttpFoundation\Request;

class FilmController extends Controller
{
    public function filmCreatePage(){
        $categoriesList = Category::all();

        return view('film.create', compact('categoriesList'));
        }

    public function store(Request $req, Film $film){

        $film = Film::create([
            'title'=> $req->title,
            'trailer'=> $req->trailer,
            'image' => $req->image->store('public/film/image'),
            'episode'=> $req->episode,
        ]);

        foreach($req->categoriesList as $category){
            $film->categories()->attach($category); 
        }

        return redirect('homepage');

    }

}
