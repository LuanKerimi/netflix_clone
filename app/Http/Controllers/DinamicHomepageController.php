<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Category;
use App\Models\HomeCategoryIndex;
use Illuminate\Http\Request;

class DinamicHomepageController extends Controller
{

    public function edit(){
        $categoryList = Category::all();
        return view('dinamicHomepage.editPage', compact('categoryList'));
    }
    
    public function homepage(){
        
        $homeCategoryIndex = HomeCategoryIndex::first();
        // $categoryIndex = [2];
        $categoryIndex = explode(',', $homeCategoryIndex->categoryByIndex);
        
        $categoryIndexStr = implode(',', $categoryIndex);
        $categoryList = Category::whereIn('id', $categoryIndex)
        ->orderByRaw(Category::raw("FIELD(id, $categoryIndexStr)"))
        ->get();
        
        return view('dinamicHomepage.homepage', compact('categoryList'));
    }

    public function store(Request $req){
        $indexStr = '';
        $oldIndex= HomeCategoryIndex::first();
    
        foreach($req->except('_token','recentMovies', 'topMovies') as $key=>$input){
                $indexStr .= $input .= ',';             
        }
    
        $indexStr = substr($indexStr, 0, -1); 
    
        if($oldIndex){
            $oldIndex->delete();
        }  

        $dinamicHomepage = HomeCategoryIndex::create([
            'categoryByIndex'=> $indexStr, 
            'recentMovies'=>$req->recentMovies,   
            'topMovies'=>$req->topMovies,   

        ]);
    
        return redirect(route('homepage'));
    
    }
}
