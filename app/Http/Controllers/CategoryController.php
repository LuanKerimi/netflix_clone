<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function categoryCreatePage(){
        return view('category.create');
    }

    public function categoryList(){
        $categoryList = Category::all();
        
        return view('category.categoryList', compact('categoryList'));
    }

    public function delete(Category $category){
        $category->delete();

        return redirect(route('homepage'));
    }

    public function edit(Category $category, Request $request){
        return view('category.categoryUpdate', compact('category'));
    }

    public function update(CategoryRequest $req, Category $category){
        $category->name = $req->name;
        $category->save();

        return redirect(route('homepage'));
    }


    public function store(Request $req){
        $category = Category::create([
            'name' => $req->name,
        ]);

        return redirect('');
    }
}
