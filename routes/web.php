<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DinamicHomepageController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\PublicController;
use App\Models\Category;
use App\Models\Film;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//                  CATEGORY CONTROLLER
Route::GET    ('/category/createPage',        [CategoryController::class, 'categoryCreatePage'])->name('categoryCreatePage');
Route::GET    ('/category/edit/{category}',   [CategoryController::class, 'edit'])->name('categoryEdit');
Route::delete ('/category/delete/{category}', [CategoryController::class, 'delete'])->name('categoryDelete');
Route::GET    ('/category/list',              [CategoryController::class,'categoryList'])->name('categoryList');
Route::POST   ('/category/store',             [CategoryController::class, 'store'])->name('storeCategory');
Route::PUT    ('/category/update/{category}', [CategoryController::class, 'update'])->name('categoryUpdate');

//                  DINAMIC HOME CONTROLLER
Route::GET    ('/',                [DinamicHomepageController::class, 'homepage'])->name('homepage');
Route::GET    ('/homepage/edit',   [DinamicHomepageController::class, 'edit'])->name('editHomepage');
Route::POST   ('/homepage/store',  [DinamicHomepageController::class, 'store'])->name('storeHomepage');

//                  FILM CONTROLLER

Route::GET('/film/createPage',  [FilmController::class, 'filmCreatePage'])->name('filmCreatePage');
Route::POST('/film/store',      [FilmController::class, 'store'])->name('storeFilm');

//                  PUBLIC CONTROLLER
// Route::GET('/', [PublicController::class, 'homepage'])->name('homepage');
