<x-layout>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{route('categoryUpdate', compact('category'))}}"> 
        @csrf
        @method('put')
        <div class="my-3">
            <label for="InputName" class="form-label">Scegli Nome</label>
            <input type="text" class="form-control" id="InputName" name="name" value="{{old("name")}}" placeholder="{{$category->name}}">
        </div>
        <button type="submit" class="btn btn.primary ">Aggiorna</button>      
    </form>

</x-layout>
