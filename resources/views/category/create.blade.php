<x-layout>


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{route('storeCategory')}}" enctype="multipart/form-data">
        @csrf
        <div class="my-3">
            <div hidden="category->id"></div>
            <label for="InputName" class="form-label">Inserisci nome</label>
            <input type="text" class="form-control" id="InputName" name="name">
        </div>
    </form>

</x-layout>
