<x-layout>
    <div class="container ">
        @foreach ($categoryList as $category )
        <div class="row ">

            <h4>{{$category->name}}</h4>

            <div class="col-2">
                <a href="{{route('categoryEdit', compact('category'))}}" class="">
                    <button class="btn btn-warning">Modifica</button>
                </a>
            </div>

           
            <div class="col-2">
                <form method="POST" action="{{route('categoryDelete', compact('category'))}}">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger " >Elimina</button>
                </form>


            </div>
     
        </div>
            
        @endforeach
    </div>

</x-layout>