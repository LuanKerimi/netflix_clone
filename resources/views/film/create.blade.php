<x-layout title="crea film">


    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    

   

    <form method="POST" action="{{route('storeFilm')}}" enctype="multipart/form-data">
        @csrf


        <div>
            <fieldset>
                @foreach ($categoriesList as $category)
                        <input class="form-check-input col-3 " id="flexCheckDefault" type="checkbox" name="categoriesList[]" value="{{$category->id}}" >
                        <label class="form-check-label form-txt me-1 " for="flexCheckDefault">
                            {{$category->name}}
                        </label>     
                @endforeach
            </fieldset>


        </div>
        
        <div class="my-3">
            <label for="InputTitle" class="form-label">Inserisci titolo</label>
            <input type="text" class="form-control" id="InputTitle" name="title">
        </div>

        <div class="my-3">
            <label for="InputEp" class="form-label">Inserisci numero episodi</label>
            <input type="integer" class="form-control" id="InputEp" name="episode">
        </div>

        <div class="my-3">
            <label for="InputTrailer" class="form-label">Inserisci trailer</label>
            <input type="text" class="form-control" id="InputTrailer" name="trailer">
        </div>
{{-- 
        <div class="my-3">
            <label for="IntputImage" class="form-label">Inserisci image</label>
            <input type="file" class="form-control" id="IntputImage" name="image">
        </div> --}}

        <div class="form-group mb-2">
            <label for="exampleInputEmail1">Foto</label>
            <input type="file" class="form-control" name="image">
          </div>

        <button type="submit" class="btn btn-primart">Salva</button>

    </form>





</x-layout>
